#ifndef NUKEM_CAMERA_HPP
#define NUKEM_CAMERA_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include <array>
#include "vars.hpp"
#include "Map.hpp"

namespace nukem {

class Camera : public sf::Transformable
{
    public:
        Camera(sf::RenderWindow& window, Map& map);
        virtual ~Camera();
        void update();
        void draw();
        bool intersect(unsigned int it);

    protected:

    private:

        sf::CircleShape m_shape;
        std::vector<std::array<sf::Vertex, 2>> m_2drays;
        float m_angle;
        sf::RenderWindow& m_renderWindow;
        Map m_map;
        sf::Vector2f m_intersection;
};

} // namespace nukem

#endif // NUKEM_CAMERA_HPP
