#include "nukem/Camera.hpp"

namespace nukem {

Camera::Camera(sf::RenderWindow& window, Map& map) : m_angle(0), m_renderWindow(window), m_map(map)
{
    m_2drays.resize(SCREEN_WIDTH);
    m_shape.setRadius(10);
    m_shape.setOrigin(10, 10);
    m_shape.setFillColor(sf::Color::Blue);
}

void Camera::update()
{

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
    {
        m_angle -= 5;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
    {
        m_angle += 5;
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
    {
        move(5*dcos(m_angle), 5*dsin(m_angle));
    }
    if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
    {
        move(-5*dcos(m_angle), -5*dsin(m_angle));
    }

    for(unsigned int i = 0; i < m_2drays.size(); ++i)
    {
        float angle = -(float)i*delta + disc + m_angle;
        m_2drays[i][0] = sf::Vertex(getPosition(), {255, 0, 0});
        if(intersect(i))
        {
            m_2drays[i][1] = sf::Vertex(m_intersection, {255, 0, 0});
        }
        else m_2drays[i][1] = sf::Vertex({getPosition().x+RAY_SIZE*dcos(angle), getPosition().y+RAY_SIZE*dsin(angle)}, {255, 0, 0});
    }
}

Camera::~Camera()
{}

void Camera::draw()
{
    sf::RenderStates states = sf::RenderStates::Default;
    states.transform *= getTransform();
    m_renderWindow.draw(m_shape, states);
    for(const auto& line : m_2drays)
    {
        m_renderWindow.draw(&line[0], 2, sf::Lines);
    }
}

bool Camera::intersect(unsigned int it)
{
    float fAngle = (m_angle+FOV/2-it*delta);
    sf::Vector2f direction = {dcos(fAngle), dsin(fAngle)};

    for(unsigned int l = 0; l < RAY_SIZE; ++l)
    {
        int dx = static_cast<int>(getPosition().x + l * direction.x);
        int dy = static_cast<int>(getPosition().y + l * direction.y);

        if(m_map.checkCase(dx/BLOCK_SIZE, dy/BLOCK_SIZE))
        {
            m_intersection.x = dx;
            m_intersection.y = dy;
            return true;
        }
    }

    return false;
}


} // namespace nukem
