#ifndef NUKEM_VARS_HPP
#define NUKEM_VARS_HPP

namespace nukem {

/**DEVELOPER : YOU CAN CHANGE THOSE NUMBERS*/
const unsigned int SCREEN_WIDTH = 640;
const unsigned int SCREEN_HEIGHT = 480;
const float FOV = 60;
const float RAY_SIZE = 350;

const unsigned int MAP_BLOCKX = 20;
const unsigned int MAP_BLOCKY = 15;
const unsigned int BLOCK_SIZE = 32;

/**DON'T CHANGE THOSE ONES**/
const float delta = FOV/(SCREEN_WIDTH-1);
const float disc = FOV/2.0f;
const float pi = 3.141592653f;

/**WOOOOO MATHS!**/
template<class N>
N dcos(N val) //Cosine function using degrees
{
    return std::cos((val*2*pi)/360);
}

template<class N>
N dsin(N val) //Sine function using degrees
{
    return std::sin((val*2*pi)/360);
}

} // namespace nukem

#endif // NUKEM_VARS_HPP
