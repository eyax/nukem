#include <nukem/Map.hpp>
#include <nukem/Camera.hpp>

using namespace nukem;

int main()
{
    sf::RenderWindow window2D({SCREEN_WIDTH, SCREEN_HEIGHT}, "Raycast Engine Demo");
    sf::Image image;
    image.loadFromFile("icon.png");
    window2D.setIcon(16, 16, image.getPixelsPtr());
    window2D.setFramerateLimit(60);

    Map map(window2D);
    Camera camera(window2D, map);
    camera.setPosition(150, 150);

    while(window2D.isOpen())
    {
        sf::Event e;
        while(window2D.pollEvent(e))
        {
            if(e.type == sf::Event::Closed) window2D.close();
        }

        camera.update();

        window2D.clear({0, 0, 0});

        map.draw();
        camera.draw();

        window2D.display();

    }

    return 0;
}
