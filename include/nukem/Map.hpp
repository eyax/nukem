#ifndef NUKEM_MAP_HPP
#define NUKEM_MAP_HPP

#include <SFML/Graphics.hpp>
#include <vector>
#include "vars.hpp"

namespace nukem {

class Map
{
    public:
        Map(sf::RenderWindow& renderWindow);
        virtual ~Map();
        void draw();
        void update();
        bool checkCase(unsigned int x, unsigned int y);

    protected:

        sf::RenderWindow& m_renderWindow;
        std::vector<std::vector<unsigned int>> m_vecMap;
        std::vector<sf::RectangleShape> m_vecBox;

    private:
};

} // namespace nukem

#endif // NUKEM_MAP_HPP
