#include "nukem/Map.hpp"

namespace nukem {

Map::Map(sf::RenderWindow& renderWindow) :
    m_renderWindow(renderWindow)
{
    m_vecMap =
    {
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,1,1,1,0,0,1,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,1},
        {1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1},
        {1,1,1,1,1,0,0,0,0,1,0,0,0,1,0,0,0,0,0,1},
        {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1},
        {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}
    };

    for(unsigned int y = 0; y < MAP_BLOCKY; ++y)
    {
        for(unsigned int x = 0; x < MAP_BLOCKX; ++x)
        {
            if(checkCase(x, y))
            {
                sf::RectangleShape box({BLOCK_SIZE, BLOCK_SIZE});
                box.setPosition({(float)x*BLOCK_SIZE, (float)y*BLOCK_SIZE});
                box.setFillColor(sf::Color::White);
                m_vecBox.push_back(box);
            }
        }
    }
}

Map::~Map()
{
    //dtor
}

void Map::draw()
{
    for(auto& vvec : m_vecBox)
    {
        m_renderWindow.draw(vvec);
    }
}

void Map::update()
{

}

bool Map::checkCase(unsigned int x, unsigned int y)
{
    if(x >= 0 && x < MAP_BLOCKX && y >= 0 && y < MAP_BLOCKY)
        return m_vecMap[y][x] != 0;
    return false;
}

} // namespace nukem
